'use strict';

// Define the `carApp` module
angular.module('carApp', [
  'ngAnimate',
  'ngRoute',
  'core',
  'car',
  'carList',
  'carDetail',
  'carManage',
]);
