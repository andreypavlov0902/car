'use strict';

// Register `carList` component, along with its associated controller and template
angular.
  module('carList').
  component('carList', {
    templateUrl: 'car-list/car-list.template.html',
    controller: ['CarService', '$location',
      function PhoneListController(carService, $location) {
        this.selectedCar = null;
        this.cars = carService.cars;

        this.clickCarInf = function clickCarInf(car) {
          this.selectedCar = car;
        };

        this.clickEditCar = function clickCarInf(car) {
          carService.addCar(car);
          $location.path('/edit-car/' + car.carId);
        };

        this.redirectToCreateCar = function () {
          $location.path('/create-car');
        };

        this.clearSelectedCar = function () {
            this.selectedCar = null;
        };
      }
    ]
  });
