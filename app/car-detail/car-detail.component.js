'use strict';

// Register `car` component, along with its associated controller and template
angular.
module('carDetail').
component('carDetail', {
    templateUrl: 'car-detail/car-detail.template.html',
    bindings: {
        selectedCar: '<',
        handlerEditClick: '&',
        handlerClearSelectedCar: '&',
    },
    controller: [
        function carDetailController() {
            this.clickEditCar = function clickCarInf() {
                this.handlerEditClick({car: this.selectedCar});
            };
            this.clearSelectedCar = function () {
                this.handlerClearSelectedCar({car: null});
            };
        }
    ]
});
