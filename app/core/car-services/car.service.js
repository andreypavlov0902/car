'use strict';

angular.
  module('core.carService').
  factory('CarService', [
    function() {
      return {
        carId: 0,
        incrementCarId: function() {this.carId++;},
        cars: null,
        addCar: function(newCar){
          if (!this.cars) {
            this.cars = [Object.assign({}, newCar)];
          } else {
            var oldCar = this.getCarById(newCar.carId);
            if (!oldCar) {
              this.cars.push(Object.assign({}, newCar));
            }
          }
        },
        updateCar: function(car){
          var index = this.cars.findIndex(function (item) {
            return item.carId == car.carId;
          });
          if (index || index === 0) {
            this.cars[index] = this.getNewObject(car);
          }
        },
        getCarById: function (carId) {
          if (!this.cars) {
            return;
          }
          return this.cars.find(function (car)  {
            return car.carId == carId;
          });
        },
        getNewObject: function (data) {
          return Object.assign({}, data);
        },
      };
      },
  ]);
