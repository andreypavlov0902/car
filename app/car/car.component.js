'use strict';

// Register `car` component, along with its associated controller and template
angular.
module('car').
component('car', {
    templateUrl: 'car/car.template.html',
    bindings: {
        car: '<',
        handlerClickCar: '&',
    },
    controller: [
        function CarController() {
            this.clickCarInf = function () {
                this.handlerClickCar({car: this.car});
            };
        }
    ]
});
