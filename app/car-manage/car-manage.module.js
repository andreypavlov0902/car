'use strict';

// Define the `carManage` module
angular.module('carManage', [
    'ngRoute',
    'core.carService',
]);
