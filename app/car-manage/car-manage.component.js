'use strict';

// Register `carManage` component, along with its associated controller and template
angular.
module('carManage').
component('carManage', {
    templateUrl: 'car-manage/car-manage.template.html',
    controller: [
        '$routeParams', '$location',  'CarService',
        function CarManageController( $routeParams, $location, carService) {
            this.isEdit = false;
            this.carForm = {
                carName: null,
                vehicleId: null,
                model: null,
                type: null,
                year: null,
                carId: carService.carId
            };
            carService.incrementCarId();

            if ($routeParams.carId) {
                var car = carService.getCarById($routeParams.carId);
                this.carForm = carService.getNewObject(car);
                this.isEdit = true;
            }

            this.handlerSave = function () {
                if (!this.isEdit) {
                    carService.addCar(this.carForm);
                } else {
                    carService.updateCar(this.carForm);
                }
                $location.path('/cars/');
            };

            this.handlerTab = function (evt, cityName) {
                var i, tabcontent, tablinks;

                // Get all elements with class="tabcontent" and hide them
                tabcontent = document.getElementsByClassName("tabcontent");
                for (i = 0; i < tabcontent.length; i++) {
                    tabcontent[i].classList.add("none");
                }

                // Get all elements with class="tablinks" and remove the class "active"
                tablinks = document.getElementsByClassName("tablinks");
                for (i = 0; i < tablinks.length; i++) {
                    tablinks[i].className = tablinks[i].className.replace(" active", "");
                }

                // Show the current tab, and add an "active" class to the button that opened the tab
                document.getElementById(cityName).classList.remove("none");
                document.getElementById(cityName).classList.add("block");
                evt.currentTarget.className += " active";
            };
        }
    ]
});
