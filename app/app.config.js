'use strict';

angular.
  module('carApp').
  config(['$routeProvider',
    function config($routeProvider) {
      $routeProvider.
        when('/cars', {
          template: '<car-list></car-list>'
        }).
          when('/create-car', {
         template: '<car-manage></car-manage>'
      }).
      when('/edit-car/:carId', {
        template: '<car-manage></car-manage>'
      }).
        otherwise('/cars');
    }
  ]);
